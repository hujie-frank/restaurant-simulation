## Requirement
`pip install -r requirements.txt`

## PART1: EXPLORATORY DATA ANALYSIS
#### Task 1: Make a plot of the distribution of the cost in each course.
> `python part1_plot.py`

#### Task 2: Make a barplot of the cost per course.
> `python part1_barplot.py`
#### Task 3: Determine the cost of the drinks per course and create 6 additional columns.
> `python part1_bill.py`

> The actual food and the cost of the drinks that was bought for each course are recorded in part1_food_drinks.csv.
## PART2: CLUSTERING DATA
> `python part2_cluster.py`

> The labels are written in part1_group.csv.

> * label 0: Onetime Group
> * label 1: Retirement Group
> * label 2: Business Group
> * label 3: Healthy Group

## PART3: DETERMINING DISTRIBUTIONS
#### Task 1: Determine the distribution of clients.
> `python part1_client_dist.py`
#### Task 2: Determine the likelihood for each of these clients to get a certain course.
> `python part1_course_dist.py`
#### Task 3: Determine the probability of a certain type of customer ordering a certain dish.
> `python part1_dish_dist.py`
#### Task 4: Determine the distribution of dishes, per course, per customer type.
> `python part1_dish_course_dist.py`
#### Task 5: Determine the distribution of the cost of the drinks per course.
> `python part1_drink_dist.py`


