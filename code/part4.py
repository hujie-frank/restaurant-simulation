from http import client
from logging import raiseExceptions
from numbers import Rational
import os
import csv
from time import time
from matplotlib import pyplot as plt
import random


COURSES={0: 'First Course', 1: 'Second Course', 2: 'Third Course'}
MENU = {'Soup': 3, 'Tomato-Mozarella': 15, 'Oysters': 20,
         'Salad': 9, 'Spaghetti': 20, 'Steak': 25, 'Lobster': 40,
         'Ice cream': 15, 'Pie': 10} 

CLIENTS={0: 'Onetime', 1: 'Retirement', 2: 'Business', 3: 'Healthy'}


def index_sampler(ratios):
    randomnum = random.random()
    summation = 0
    index = 0
    for index in range(len(ratios)):
        summation += ratios[index]
        if summation >= randomnum:
            break
    return index

def client_type_sampler():
    ratios = [0.5186, 0.1143, 0.2168, 0.1503]
    index = index_sampler(ratios)    
    return CLIENTS[index], index

def read_client_id():
    infile = os.path.join('..', 'part1_group.csv')
    type_dict = {0:[], 1:[], 2:[], 3:[]}
    with open(infile) as f:
            row = csv.reader(f, delimiter = ',')
            header = next(row)  # read first line
            for r in row:
                type_dict[int(r[-1])].append(r[0])
    return type_dict
client_type_dict = read_client_id()


def client_id_sampler(client_type_dict, client_type_id):
    client_ids = client_type_dict[client_type_id]
    index = random.randint(0, len(client_ids) - 1)
    return client_ids[index]


def course_sampler(client_type_id):
    all_ratios = [[0.21671198409155473, 0.6366624731139159, 0.14662554279452944], 
              [0.3832107532335785, 0.35277707329444585, 0.26401217347197564], 
              [0.480529982582732, 0.0944264742473252, 0.42504354316994275], 
              [0.5720024721878862, 0.09505562422744128, 0.33294190358467246]]
    ratios = all_ratios[client_type_id]
    index = index_sampler(ratios)
    return COURSES[index], index

def food_sampler(client_type_id, course_id):
    all_ratios = [[{'Soup': 1.0}, 
                   {'Oysters': 0.17126254622292658, 'Steak': 0.6646592709984153, 'Lobster': 0.1640781827786582}, 
                   {'Pie': 0.9200407435701553, 'Tomato-Mozarella': 0.07995925642984467}], 
                  [{'Oysters': 0.8580067731011127, 'Soup': 0.09361393323657474, 'Tomato-Mozarella': 0.04837929366231253}, 
                   {'Lobster': 1.0}, 
                   {'Pie': 0.8867120954003407, 'Tomato-Mozarella': 0.11328790459965929}], 
                  [{'Soup': 1.0}, 
                   {'Pie': 0.8052571717427018, 'Salad': 0.1918362188803235, 'Oysters': 0.0029066093769745988}, 
                   {'Tomato-Mozarella': 0.18838028169014084, 'Pie': 0.8116197183098591}], 
                  [{'Tomato-Mozarella': 0.325309992706054, 'Oysters': 0.6668490153172867, 'Soup': 0.007840991976659373}, 
                   {'Oysters': 0.5857038657913931, 'Steak': 0.2448942377826404, 'Salad': 0.03555798687089715, 'Pie': 0.13384390955506928}, 
                   {'Pie': 0.8927475057559479, 'Tomato-Mozarella': 0.10725249424405219}]]
    food_dict = all_ratios[client_type_id][course_id]
    food_names = list(food_dict.keys())
    food_ratios = list(food_dict.values())
    index = index_sampler(food_ratios)
    return food_names[index]

def get_drink_dist():
    def read_data(infile):
        with open(infile) as f:
            row = csv.reader(f, delimiter = ',')
            header = next(row)  # read first line
            data = []
            for r in row:
                data.append(r)
        return data
    data = read_data(os.path.join('..', 'part1_food_drinks.csv'))
    costs = [[], [], []]
    idx = 0
    for items in data:
        if len(items) == 0:
            continue
        three_course_drink = [items[6], items[8], items[10]]
        for course_id, cost in enumerate(three_course_drink):
            if cost != '' and float(cost) != 0:
                costs[course_id].append(float(cost))
        idx += 1
    return costs
drink_costs = get_drink_dist()

def drink_sampler(costs, course_id):
    index = random.randint(0, len(costs[course_id])-1)
    return costs[course_id][index]

def generate_one_day():
    result = []

    time_id = random.randint(0, 1)
    if time_id == 0:
        time_type = 'LUNCH'
    else:
        time_type = 'DINNER'
    
    client_type, client_type_id = client_type_sampler()
    client_id = client_id_sampler(client_type_dict, client_type_id)
    result.extend([time_type, client_id, client_type])


    course_sample_results = []
    for index in range(3):
        course_name, course_id = course_sampler(client_type_id)
        course_sample_results.append(course_id)
    
    course_food_list = []
    drink_cost_list = []
    total_cost_list = []

    for course_id in range(3):
        if course_id in course_sample_results:
            food_name = food_sampler(client_type_id, course_id)
            food_cost = MENU[food_name]
            drink_cost = drink_sampler(drink_costs, course_id)
            course_food_list.append(food_name)
            drink_cost_list.append(drink_cost)
            total_cost_list.append(food_cost + drink_cost)   
        else:
            course_food_list.append('')
            drink_cost_list.append(0)
            total_cost_list.append(0)

    result.extend(course_food_list)
    result.extend(drink_cost_list)
    result.extend(total_cost_list)

    return result


if __name__ == '__main__':
    outfile = os.path.join('..', 'part4.csv')
    with open(outfile, 'w') as f:
        writer = csv.writer(f, dialect='excel', lineterminator='\n')
        header = ['TIME', 'CUSTOMERID', 'CUSTOMERTYPE', 'COURSE1', 'COURSE2', 'COURSE3', 
                  'DRINKS1', 'DRINKS2', 'DRINKS3', 'TOTAL1', 'TOTAL2', 'TOTAL3']
        writer.writerow(header)
        for day_id in range(365 * 5):
            items = generate_one_day()
            writer.writerow(items)


        
    
        







    








