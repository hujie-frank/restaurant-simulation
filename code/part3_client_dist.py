import os
import csv

import numpy as np
from matplotlib import pyplot as plt


CLIENTS={0: 'Onetime', 1: 'Retirement', 2: 'Business', 3: 'Healthy'}

def read_data(infile):
    with open(infile) as f:
        row = csv.reader(f, delimiter = ',')
        header = next(row)  # read first line
        data = []
        for r in row:
            r[2:] = map(float, r[2:])
            data.append(r)
    return data

if __name__ == '__main__':
    data = read_data(os.path.join('..', 'part1_group.csv'))
    group_ids = [int(item[-1]) for item in data if len(item)!=0]
    counts = [group_ids.count(idx) for idx in range(4)]
    for idx in range(4):
        print('{}: {}'.format(CLIENTS[idx], counts[idx]))
    plt.pie(counts, labels=[CLIENTS[0], CLIENTS[1], CLIENTS[2], CLIENTS[3]])
    plt.show()