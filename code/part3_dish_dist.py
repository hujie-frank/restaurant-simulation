import os
import csv
from matplotlib import pyplot as plt


CLIENTS={0: 'Onetime', 1: 'Retirement', 2: 'Business', 3: 'Healthy'}

def read_data(infile):
    with open(infile) as f:
        row = csv.reader(f, delimiter = ',')
        header = next(row)  # read first line
        data = []
        for r in row:
            data.append(r)
    return data

def plot(likelihoods):
    plt.figure(figsize=(8, 8))
    for idx in range(4):
        plt.subplot(2, 2, idx+1)
        plt.title(CLIENTS[idx])
        plt.pie(likelihoods[idx].values(), labels=likelihoods[idx].keys())
    plt.show()

if __name__ == '__main__':
    data = read_data(os.path.join('..', 'part1_food_drinks.csv'))
    group_ids = [int(item[-1]) for item in read_data(os.path.join('..', 'part1_group.csv')) if len(item)!=0]
    counts = {0:{}, 1:{}, 2:{}, 3:{}}
    total_courses = {0:0, 1:0, 2:0, 3:0}
    idx = 0
    for items in data:
        if len(items) == 0:
            continue
        three_course_food = [items[5], items[7], items[9]]
        for course_food in three_course_food:
            if course_food != '':
                group_id = group_ids[idx]
                total_courses[group_id] += 1
                if course_food not in counts[group_id]: 
                    counts[group_id][course_food] = 1
                else:
                    counts[group_id][course_food] += 1
        idx += 1

    results = []
    for i in range(4):
        probs = {}
        for food, num in counts[i].items():
            probs[food] = num / float(total_courses[i])
        results.append(probs)
    print(results)
    plot(results)
        
    


